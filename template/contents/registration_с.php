<?php
?>
<form method="POST">
    

<div class="form-group">
  <label for="email">email</label>
  <input type="email" name="email" required class="form-control" id="email" aria-describedby="email">
  
</div>
<div class="form-group">
  <label for="password">Пароль</label>
  <input type="password" required name="password" class="form-control" id="password">
  <?php $controller->check_password(); ?>
</div>
<div class="form-group">
  <label for="password2">Повторите пароль</label>
  <input type="password" required name="password2" class="form-control" id="password2">
  <?php $controller->check_password(); ?>
</div>

<button type="submit" class="btn btn-primary">Регистрация</button>
</form>
