<?php
?>
<div class = "row" > <div class="col-sm">
    <div class="company_card">
        <div class="company_card_name">
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <div class="name">
                            <b><?php echo $data['name'] ?></b>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="add_comment float-right">
                            <b>+</b>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="count_comment float-right">
                            <b class="count_text"><?php echo count($controller->model->get_comment($_POST['id'],$_SESSION['uuid'])); ?></b>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="add_comment_text d-none">
            <div class="container">
            <div class="row">
                    <div class="col-sm">
                        <div class="head_text_2">
                            Добавить комментарий
                        </div>
                    </div>
                </div>    
            <div class="row">
                    <div class="col-sm">
                        <b>Комментарий</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <textarea id="text_comment"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                    <button type="button" id="bt_save_comment" class="btn btn-primary">Добавить</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="comment_text2 d-none">
        </div>
        <div class="company_card_information">
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <b>ИНН</b>
                    </div>
                    <div class="col-sm">
                        <?php echo $data['INN'] ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <b>Информация</b>
                    </div>
                    <div class="col-sm">
                        <?php echo $data['information'] ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <b>Директор</b>
                    </div>
                    <div class="col-sm">
                        <?php echo $data['director'] ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <b>Адрес</b>
                    </div>
                    <div class="col-sm">
                        <?php echo $data['address'] ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <b>Телефон</b>
                    </div>
                    <div class="col-sm">
                        <?php echo $data['phone'] ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <b>Дата создания</b>
                    </div>
                    <div class="col-sm">
                        <?php echo date("d-m-Y H:i:s",strtotime($data['insert_date'])) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    $(document).ready(function () {
        $(".add_comment").click(function () {
            if ($(".add_comment_text").hasClass("d-none")) {
                $(".add_comment_text").removeClass("d-none")
               
            } else {
                $(".add_comment_text").addClass("d-none")
            }
        })
        $("#bt_save_comment").click(function () {
            let data = {};
            if ($("#text_comment").val() == "") {
                alert("Напишите комментарий");
            } else {
                data.text = $("#text_comment").val();
                data.method = "add_comment";
                $.post(location.href, data, function (res) {
                    alert("Успешно добавлено");
                    let c = $(".count_comment .count_text").html();
                    $(".count_comment .count_text").html(Number(c) + 1);
                    $(".add_comment_text").addClass("d-none")
                    let data = {};
                    data.method = "get_comment";
                    $.post(location.href, data, function (res) {
                        $(".comment_text2").html(res);
                    })
                })
            }

        })
        $(".count_comment").click(function () {
            let data = {};
            data.method = "get_comment";
            if ($(".comment_text2").hasClass("d-none")) {
                $(".comment_text2").removeClass("d-none")
            } else {
                $(".comment_text2").addClass("d-none")
            }
            $.post(location.href, data, function (res) {
                $(".comment_text2").html(res);
            })
        })
    })
</script>