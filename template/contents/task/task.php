<div class="task container" id="<?php echo $id ?>">    
<div class="form-group">
        <label for="name">Имя</label>
        <input
            type="text"
            class="form-control"
            name="name"
            required="required"
            <?php echo (!$write)? 'readonly' : "" ?>
            id="name"
            value="<?php echo $name ?>"
            placeholder="Введите имя">

    </div>
    <div class="form-group">
        <label for="status">Статус</label>
        <select <?php echo (!$write_status)? 'disabled' : "" ?>  class="form-control" id="status" name="status">
        <option value="-----">---</option>    
        <option value="not_done">Не выполнено</option>
            <option value="done">Выполнено</option>
            <option value="await">Ожидание</option>
        </select>
        <script>
          
            $(".task#<?php echo  $id ?>").find("#status option[value='<?php echo $status ?>']").attr("selected","true");
        </script>
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input
            type="email"
            class="form-control"
            name="email"
            required="required"
            <?php echo (!$write)? 'readonly' : "" ?>
            id="email"
            value="<?php echo $email ?>"
            placeholder="Введите Email">

    </div>
    <div class="form-group">
        <label for="task_text">Текст задачи</label>
        <textarea
            type="text"
            class="form-control"
            name="task_text"
            required="required"
            <?php echo (!$write)? 'readonly' : "" ?>
            id="task_text"
            placeholder="Введите текст задачи"><?php echo $text ?></textarea>
        <input type="hidden" name="method" value="<?php echo $method ?>">
        <input type="hidden" name="id" value="<?php echo $id ?>">
    </div>
    <smail><?php echo (@$edit=="admin")?"отредактировано администратором":"" ?></smail>

</div>
