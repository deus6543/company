
<div class="container">
    <div class="row">
        <div class="col-sm">
            <div class="head_text_2">
                Комментарии
            </div>
        </div>
    </div>
<?php foreach($comment as $c): ?>    
<div class="row comment" id="<?php echo $c['id'] ?>">
        <div class="container">
            <div class="row">
                <div class="col-5">
                    <div class="user"><b><?php echo $model->get_email_user($c['author'])?></b></div>
                </div>
                <div class="col-sm">
                    <div class="date"><b><?php echo date("d-m-Y H:i:s",strtotime($c['insert_date'])) ?></b></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm">
                    <div class="text"><em><?php echo $c['text'] ?></em></div>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach ?>

</div>