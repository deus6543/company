<?php

?>
<div class="row">
    <div class="col-sm">
        <div class="head_text">
            Компании
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm">
        <div class="area_company">
            <?php foreach($data as $d): ?>
            <div class="link_company">

                <a href="company/<?php echo $d['id'] ?>">
                    <div class="head_text_3">
                        <?php echo $d['name']?>
                    </div>
                    <div class="description">
                        <?php echo $d['information']?>
                    </div>
                </a>

            </div>
            <?php endforeach ?>
        </div>

    </div>

</div>

<div class="row">
    <div class="col-sm">
        <div class="head_text_2">
            Добавить компанию
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm">
        <div class="add_company">
        <form method="POST">    
        <div class="form-group">
                <label for="name">Название</label>
                <input
                    type="text"
                    class="form-control"
                    id="name"
                    name="name"
                    aria-describedby="name"></input>
            </div>
        <div class="form-group">
                <label for="INN">ИНН</label>
                <input
                    type="number"
                    class="form-control"
                    id="INN"
                    name="INN"
                    aria-describedby="INN"></input>
            </div>
            <div class="form-group">
                <label for="information">Информация</label>
                <textarea
                    type="Text"
                    class="form-control"
                    id="information"
                    name="information"
                    aria-describedby="information"></textarea>
            </div>
            <div class="form-group">
                <label for="director">Директор</label>
                <input
                    type="text"
                    class="form-control"
                    id="director"
                    name="director"
                    aria-describedby="director"></input>
            </div>
            <div class="form-group">
                <label for="address">Адрес</label>
                <input
                    type="text"
                    class="form-control"
                    id="address"
                    name="address"
                    aria-describedby="address"></input>
            </div>
            <div class="form-group">
                <label for="phone">Телефон</label>
                <input
                    type="number"
                    class="form-control"
                    id="phone"
                    name="phone"
                    aria-describedby="phone"></input>
                    <input type="hidden" name="method" value="add_company">
            </div>
            <button type="submit" class="btn btn-primary">Добавить</button>
            </form>
        </div>
    </div>
</div>