<?php
class Controller{
    public $view;
    public $model;
    public $mysqli;
    function __construct()
	{
        $this->view = new View();
        $this->mysqli = new bd_mysql();
        $this->model=new Model();
       
    }

    //Добавляем компанию
    public function add_company()
    {
        unset($_POST['method']);
        $data=$_POST;
        $data['author']=$_SESSION['uuid'];
        $this->mysqli->insert("company",$data,"auto");

    }
    //добавляем коммент
   public function add_comment()
   {
    $data['author']=$_SESSION['uuid'];
    $data['text']=$_POST['text'];
    $data['id_company']=$_POST['id'];
   
    $this->mysqli->insert("comment",$data,"auto");
   }
    
    //действие
    public function action_index($content_view, $template_view, $data = null)
	{
        $controller=new Controller;
        $this->view->generate($content_view, $template_view,$data, $controller);
    }
    //обработка взаимодействия
    //логинемся
    public function login()
    {
      
       $res=$this->mysqli->get_row("SELECT * FROM login where email='".@$_POST['email']."' and password='".crypt($_POST['password'],$_POST['email'])."'");
       if( $res)
       {
        $_SESSION['auth']=true;
        $_SESSION['uuid']=$res['uuid'];
        $_SESSION['email']=$res['email'];
       }
       else
       {
        $_SESSION['auth']=false;
       }
       
       
    }
    //проверка паролей
    public function check_password(){
        if(isset($_SESSION["error"]["check_password"]))
        {
            echo " <smail class='error'> Пароли не совпадают </smail>";
        }
       
    }
    //установка ошибки
    public function set_error_check_password()
    {
        if(!empty($_POST['password']) && !empty($_POST['password2']) && $_POST['password']!=$_POST['password2'])
        {
            if(isset($_SESSION["error"]["check_password"]))
            {
                $_SESSION["error"]["check_password"]["count"]++;    
            }
            else{
                $_SESSION["error"]["check_password"]["text"]="check_password";
                $_SESSION["error"]["check_password"]["count"]=0;
            }
         
            
            $_SESSION["registration"]=false;
   
        }
        else
        {
            $_SESSION["error"]=false;
            if(!isset($_POST['password']) || !isset($_POST['password2']))
            {
                $_SESSION["registration"]="new";
            }
            else{
                $_SESSION["registration"]=true;
            }
            
        }
    }
    public function generate_uuid()
    {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
       
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        mt_rand( 0, 0xffff ),
       
        mt_rand( 0, 0x0fff ) | 0x4000,

        mt_rand( 0, 0x3fff ) | 0x8000,

        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
    }
    //регистрация
    public function registration()
    {
        if(isset($_POST['email']) && isset($_POST["password"]))
        {
            $data['email']=$_POST['email'];
            $data['password']=crypt($_POST['password'],$_POST['email']);

            $data['uuid']=$this->generate_uuid();
            $this->mysqli->insert("login",$data,"auto");
            $_SESSION["registration"]="new";
            $_SESSION["error"]=false;
        }
       
    }
}